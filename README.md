# [Nix-portable](https://github.com/DavHau/nix-portable) wrapper for [direnv](https://github.com/direnv/direnv/) integration
## TODO: check if [Nixie](https://github.com/orgs/nixie-dev/discussions/8) might be a better tool

This is for my friend who really doesn't want to install nix on his machine...

It creates a one-liner to launch a `nix develop` shell:

[![asciicast](https://asciinema.org/a/535254.svg)](https://asciinema.org/a/535254)

## Usage

### `.envrc`
```bash
if ! has nix; then
  # Polyfill nix (experimental :P) - see https://gitlab.com/txlab/tools/nix-portable-direnv/
  echo
  echo "$(tput bold; tput setaf 1)Nix is not installed$(tput sgr0) - fetching nix-portable wrapper (experimental)"
  NIX_PORTABLE_WRAPPER=$(fetchurl https://gitlab.com/txlab/tools/nix-portable-direnv/-/raw/main/nix-portable-wrapper.sh \
    'sha256-V/+TtUx4NdjJemBLdzJgGk4WUo1yAs2a/qCyk3Degas=')
  mkdir -p .direnv/bin
  cp "$NIX_PORTABLE_WRAPPER" .direnv/bin/nix-portable
  chmod u+rwx .direnv/bin/nix-portable
  PATH_add "$(expand_path .direnv/bin)"

  echo "$(tput bold)Run this command to drop into a pure nix shell$(tput sgr0) (if you want integration with your shell, you will have to install nix 🤷)"
  echo "-> $(tput smul)nix-portable$(tput sgr0)"
  echo

else
  # Normal behaviour for people with nix installed
  if [[ $(type -t use_flake) != function ]]; then
    echo "ERROR: use_flake function missing."
    echo "Please update direnv to v2.30.0 or later."
    exit 1
  fi
  use flake
fi
```
