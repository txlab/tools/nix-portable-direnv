#!/usr/bin/env bash
set -euo pipefail

########################################################
## SOURCE & DOCS:                                     ##
## https://gitlab.com/txlab/tools/nix-portable-direnv ##
########################################################

if [[ -z "$DIRENV_DIR" ]]; then
    echo "Direnv is not loaded correctly (missing \$DIRENV_DIR)" >&2
    exit 1
fi

{
    cd "${DIRENV_DIR#-}"
    #direnv stdlib | source /dev/stdin
    echo "Nix is not installed - fetching nix-portable (experimental)"
    NIX_PORTABLE=$(direnv fetchurl https://github.com/DavHau/nix-portable/releases/download/v009/nix-portable \
        'sha256-43Hcd9jNtP76zdLYq/m1zgE7s4Sis33ve5bo3LDXd5A=')
    #echo "Copying $NIX_PORTABLE to .direnv/bin/nix"
    mkdir -p .direnv/bin
    cp "$NIX_PORTABLE" .direnv/bin/nix
    chmod u+rwx .direnv/bin/nix
    # PATH_add "$(expand_path .direnv/bin)" - already done in .envrc
}

echo "Dropping into a pure nix shell (if you want integration with your shell, you will have to install nix 🤷)" # TODO: for now... 😈 - issue #1
echo "-> https://nixos.org/download.html"
echo
nix develop
